extern crate converter_lib;
use std::fs;
use std::path::PathBuf;
use converter_lib::command::contract_command::{ContractCommand, ContractCommandBuilder};

fn build_command() -> ContractCommand {
    let mut builder = ContractCommandBuilder::default();
    let cargo_dir = env!("CARGO_MANIFEST_DIR");
    let mut resource_folder = PathBuf::from(cargo_dir);
    resource_folder.push("resources");
    let mut contract = resource_folder.clone();
    let mut json = resource_folder.clone();
    contract.push("contract.groovy");
    json.push("test.json"); 
    builder.label("test".to_string())
        .triggered_by(Some("testFunc".to_string()))
        .path_to_json(json.to_str().map(|x| x.to_string()).unwrap())
        .message_from(None)
        .sent_to("test".to_string())
        .write_to(contract.to_str().map(|x| x.to_string()));
    builder.build().unwrap()
}

#[test]
fn run_command() {
    let command = build_command();
    command.run().unwrap();
    let cargo_dir = env!("CARGO_MANIFEST_DIR");
    let mut resource_folder = PathBuf::from(cargo_dir);
    resource_folder.push("resources");
    let mut new_contract = resource_folder.clone();
    let mut result = resource_folder.clone();
    result.push("result.groovy");
    new_contract.push("contract.groovy");
    let mut file = fs::read_to_string(&new_contract).unwrap();
    let mut result = fs::read_to_string(&result).unwrap();
    file.retain(|c| !c.is_whitespace());
    result.retain(|c| !c.is_whitespace());
    fs::remove_file(&new_contract).unwrap();
    assert_eq!(file, result);
}
