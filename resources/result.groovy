import org.springframework.cloud.contract.spec.Contract


def datetime_regex = "(?:[0-9]{4})(?:0[1-9]|1[0-2])(?:0[1-9]|[1-2][0-9]|3[0-1])(?:2[0-3]|[01][0-9])(?:[0-5][0-9])(?:[0-5][0-9])(?:\\.[0-9]+)?[+|-](?:0[0-9]|1[0-4])[0-9]{2}"; 

def uuid_regex = "/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i"; 


Contract.make  {
    label("test")
    input {
        
            triggeredBy('testMethod()')
        
    }    
    outputMessage {
        sentTo 'test-queue'
        body([
				outgoing: [
					test: $(stub("20200707021842+0700"), test(regex(datetime_regex)),
					test2: $(stub(anyNumber()), test(anyNumber())),
					test3: $(stub(anyBoolean()), test(anyBoolean())),
					test4: $(stub(anyString()), test(anyString())),
					test5: $(stub(UUID.randomUUID()), test(regex(uuid_regex))
				],
				test3Obj: [
					arr: [[
						[
							anotherArr: [[
								[
									pp: $(stub(anyString()), test(anyString()))
								]
							]],
							f123or: $(stub(anyString()), test(anyString())),
							for: $(stub(anyNumber()), test(anyNumber()))
						]
					]],
					hey: $(stub(anyString()), test(anyString())),
					nestedObj: [
						test3: $(stub(anyString()), test(anyString()))
					],
					test4: $(stub(anyString()), test(anyString()))
				]
			])
        headers {
            contentType(applicationJson())
        }
    }
}