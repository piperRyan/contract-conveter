## [0.0.3](https://gitlab.com/piperRyan/contract-conveter/compare/v1.0.0...v0.0.3) (2020-07-07)



# [1.0.0](https://gitlab.com/piperRyan/contract-conveter/compare/v0.0.3...v1.0.0) (2020-07-07)


### Bug Fixes

* fix json tags ([3cabbc1](https://gitlab.com/piperRyan/contract-conveter/commit/3cabbc1f7c67786100dbe9eea55bfe1743538212))
* fix readme markdown ([41dc9a8](https://gitlab.com/piperRyan/contract-conveter/commit/41dc9a8e01c9cc85c59fcf678d8c86e592e9b389))



## [0.0.3](https://gitlab.com/piperRyan/contract-conveter/compare/v0.0.2...v0.0.3) (2020-07-07)


### Features

* **templateGenerator:** ensure body wasn't being escaped ([52491bb](https://gitlab.com/piperRyan/contract-conveter/commit/52491bba403e8fa625ef0b7f1c76ed5c410156ef))



## [0.0.2](https://gitlab.com/piperRyan/contract-conveter/compare/v0.0.1...v0.0.2) (2020-07-07)


### Bug Fixes

* **writer:** resolve immutable borrows ([293a148](https://gitlab.com/piperRyan/contract-conveter/commit/293a14826e8756d20afbe933080476e08aa0280c))


### Features

* **rules:** add datetime rule ([0c7129f](https://gitlab.com/piperRyan/contract-conveter/commit/0c7129f15ef7bf3f6ef4e4c781a42e07fc292680))



## [0.0.1](https://gitlab.com/piperRyan/contract-conveter/compare/06e7d6050bfc8891650d4e964cb2e674f480a82f...v0.0.1) (2020-07-07)


### Features

* allow basic conversion from json to spring cloud contracts ([06e7d60](https://gitlab.com/piperRyan/contract-conveter/commit/06e7d6050bfc8891650d4e964cb2e674f480a82f))



