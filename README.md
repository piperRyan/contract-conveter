# Contract Converter

# Requirements

Rust 1.44+

# Changelog

The changelog can be found [here](CHANGELOG.md)

## How to build the project 

```powershell
cargo build --release
cp .\target\release\converter.exe .
```


## Usage

```bash
Contract Converter 0.0.3
Convert structured JSON to groovy contract

USAGE:
    conveter.exe [OPTIONS] --json-path <PATH> --label-name <LABEL> --sent-to <SENT-TO> <--message-from <MESSAGE-FROM>|--triggered-by <TRIGGERED-BY>>        

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -p, --json-path <PATH>               The file location of the json file which will be converted to a contract
    -l, --label-name <LABEL>             Set the label for this contract
    -f, --message-from <MESSAGE-FROM>    Then name of the queue that the request message is recevied from. Conflicts
                                         with triggered-by argument.
    -s, --sent-to <SENT-TO>              The name of the queue that the response message is sent to.
    -t, --triggered-by <TRIGGERED-BY>    If contract is to be triggered by a method use this argument. Provide method
                                         name without argument. Conflicts with message from option.
    -w, --write-to <PATH>                The directory to write the contract to. Defaults to the current directory the exe is in.
```

## Example

```bash
.\conveter.exe -p .\resources\test.json -l test -s test-queue -t testMethod
```

will produce 

```groovy
import org.springframework.cloud.contract.spec.Contract


def datetime_regex = "^([1-9][0-9][0-9][0-9])[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])[T]([01][1-9]|[2][0-3])[:]([0-5][0-9])[:]([0-5][0-9])([+|-]([01][0-9]|[2][0-3])[:]([0-5][0-9])){0,1}$"; 

def uuid_regex = "/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i"; 


Contract.make  {
    label("test")
    input {
        
            triggeredBy('testMethod')
        
    }    
    outputMessage {
        sentTo 'test-queue'
        body([
				outgoing: [
					test: $(stub("2020-07-07T02:18:42+0700"), test(regex(datetime_regex)),
					test2: $(stub(anyNumber()), test(anyNumber())),
					test3: $(stub(anyBoolean()), test(anyBoolean())),
					test4: $(stub(anyString()), test(anyString())),
					test5: $(stub(UUID.randomUUID()), test(regex(uuid_regex))
				],
				test3Obj: [
					arr: [[
						[
							anotherArr: [[
								[
									pp: $(stub(anyString()), test(anyString()))
								]
							]],
							f123or: $(stub(anyString()), test(anyString())),
							for: $(stub(anyNumber()), test(anyNumber()))
						]
					]],
					hey: $(stub(anyString()), test(anyString())),
					nestedObj: [
						test3: $(stub(anyString()), test(anyString()))
					],
					test4: $(stub(anyString()), test(anyString()))
				]
			])
        headers {
         contentType(applicationJson())
        }
    }
}
```


## Building the JSON

Please refer to resources/test.json as an example.

Each json must contain the following objects: 

```json
{
    "request": {},
    "response": {}
}
```

Each primitive field can be bounded to a rules so for example

```json
{
    "request": {
        "test": "uuid"
    },
    "response": {}
}
```

indicates the `test` field is a uuid and will in contract be generated as follows:  

```groovy
test: $(stub(UUID.randomUUID()), test(regex(uuid_regex))
```


### Available Rules
- boolean
- datetime
- number
- string
- uuid