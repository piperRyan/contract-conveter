use crate::rules::Parsable;
use serde_json::Value;

pub struct NumberRule {
    outgoing: String,
    incoming: String
}

impl NumberRule {
    pub fn new() -> Self {
        NumberRule {
            outgoing: "$(stub(anyNumber()), test(anyNumber()))".to_string(),
            incoming: "$(stub(anyNumber()), test(anyNumber()))".to_string()
        }
    }
}

impl Parsable for NumberRule {
    fn meets_rule(&self, json_val: &Value) -> bool {
        match json_val {
            Value::Number(ref _number) => true,
            Value::String(ref token) => {
                token.parse::<f64>().is_ok() || token.parse::<u64>().is_ok()
            }
            _ => false
        }
    }

    fn get_outgoing(&self) -> &str {
        &*self.outgoing
    }

    fn get_incoming(&self) -> &str {
        &*self.incoming
    }
}