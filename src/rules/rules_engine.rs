use crate::rules::uuid_rule::UuidRule;
use crate::rules::number_rule::NumberRule;
use crate::rules::boolean_rule::BooleanRule;
use crate::rules::string_rule::StringRule;
use crate::rules::datetime_rule::DatetimeRule;
use crate::rules::Parsable;
use crate::rules::rule_types::RuleTypes;

pub struct RulesEngine {
    string_rules: Vec<Box<dyn Parsable>>,
    boolean_rules: Vec<Box<dyn Parsable>>,
    number_rules: Vec<Box<dyn Parsable>>
}

impl RulesEngine {
    pub fn new() -> Self {
        RulesEngine {
            string_rules: vec!(
                Box::new(UuidRule::new()),
                Box::new(DatetimeRule::new()),
                Box::new(StringRule::new()),
            ),
            boolean_rules: vec!(
                Box::new(BooleanRule::new())
            ),
            number_rules: vec!(
                Box::new(NumberRule::new())
            )
        }
    }

    pub fn get_rules_by_type<'a>(&'a self, json_type: RuleTypes) -> &'a Vec<Box<dyn Parsable>> {
        match json_type {
            RuleTypes::String => &self.string_rules,
            RuleTypes::Boolean => &self.boolean_rules,
            RuleTypes::Number => &self.number_rules
        }
    }
}