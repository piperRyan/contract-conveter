use serde_json::Value;
use super::regex::Regex;
use crate::rules::Parsable;

pub struct DatetimeRule {
    regex: Option<Regex>,
    outgoing: String,
    incoming: String

}

impl DatetimeRule {
    pub fn new() -> Self {
        DatetimeRule {
            regex: Some(Regex::new("datetime_regex", "(?:[0-9]{4})(?:0[1-9]|1[0-2])(?:0[1-9]|[1-2][0-9]|3[0-1])(?:2[0-3]|[01][0-9])(?:[0-5][0-9])(?:[0-5][0-9])(?:\\\\.[0-9]+)?[+|-](?:0[0-9]|1[0-4])[0-9]{2}")),
            outgoing: "$(stub(\"20200707021842+0700\"), test(regex(datetime_regex))".to_string(),
            incoming: "$(stub(regex(datetime_regex)), test(\"20200707021842+0700\")".to_string()
        }
    }
}

impl Parsable for DatetimeRule {

    fn meets_rule(&self, val: &Value) -> bool {
        match val {
            Value::String(ref token) => {
                token.to_lowercase() == "datetime"
            }
            _ => false
        }
    }

    fn get_regex(&self) -> Option<Regex> {
        self.regex.clone()
    }

    fn get_outgoing(&self) -> &str {
        &*self.outgoing
    }

    fn get_incoming(&self) -> &str {
        &*self.incoming
    }
}

