use serde_json::Value;
use crate::rules::Parsable;

pub struct StringRule {
    outgoing: String,
    incoming: String
}

impl Parsable for StringRule {
    fn meets_rule(&self, json_val: &Value) -> bool {
        match json_val {
            Value::String(ref _token) => true,
            _ => false
        }
    }

    fn get_outgoing(&self) -> &str {
        &*self.outgoing
    }

    fn get_incoming(&self) -> &str {
        &*self.incoming
    }
}


impl StringRule {
    pub fn new() -> Self {
        StringRule {
            outgoing: "$(stub(anyString()), test(anyString()))".to_string(),
            incoming: "$(stub(anyString()), test(anyString()))".to_string()
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::Value;

    #[test]
    fn should_create_a_valid_string_rule() {
        let string_rule = StringRule::new();
        assert_eq!(string_rule.get_outgoing(), "$(stub(anyString()), test(anyString()))".to_string());
        assert_eq!(string_rule.get_incoming(), "$(stub(anyString()), test(anyString()))".to_string());
        assert_eq!(string_rule.get_regex(), None);
    }

    #[test]
    fn should_meet_rule_if_is_type_string() {
        let string_rule = StringRule::new();
        let string_type = Value::String("test".to_owned());
        assert_eq!(string_rule.meets_rule(&string_type), true);
    }

    #[test]
    fn should_not_meet_rule_if_it_not_type_string() {
        let string_rule = StringRule::new();
        let bool_type = Value::Bool(false);
        assert_eq!(string_rule.meets_rule(&bool_type), false);
    }

}

