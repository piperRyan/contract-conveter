use serde_json::Value;
use crate::rules::Parsable;

pub struct BooleanRule {
    outgoing: String,
    incoming: String,
}


impl BooleanRule {
    pub fn new() -> Self {
        BooleanRule {
            outgoing: "$(stub(anyBoolean()), test(anyBoolean()))".to_string(),
            incoming: "$(stub(anyBoolean()), test(anyBoolean()))".to_string(),
        }
    }
}

impl Parsable for BooleanRule {

    fn meets_rule(&self, json_val: &Value) -> bool {
        match json_val {
            Value::String(ref token) => {
                let indicators = vec!("boolean", "bool");
                indicators.contains(&&*token.to_lowercase())
            },
            Value::Bool(ref _token) => true,
            _ => false
        }
    }

    fn get_outgoing(&self) -> &str {
        &*self.outgoing
    }

    fn get_incoming(&self) -> &str {
        &*self.incoming
    }
}