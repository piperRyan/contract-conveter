mod uuid_rule;
mod string_rule;
mod boolean_rule;
mod number_rule;
mod datetime_rule;
pub mod rules_engine;
pub mod rule_types;
pub mod regex;
use serde_json::Value;
use regex::Regex;


pub trait Parsable {
    fn meets_rule(&self, _json_val: &Value) -> bool {
        false
    }

    fn into_rule(&self, json_val: &Value, is_out: bool) -> Option<String> {
        if !self.meets_rule(json_val) {
            None
        } else if is_out {
            Some(self.get_outgoing().to_string())
        } else {
            Some(self.get_incoming().to_string())
        }
    }

    fn  get_regex(&self) -> Option<Regex> {
        None
    }

    fn get_outgoing(&self) -> &str;

    fn get_incoming(&self) -> &str;
}