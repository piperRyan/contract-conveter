use serde_json::Value;
use super::regex::Regex;
use crate::rules::Parsable;

pub struct UuidRule {
    regex: Option<Regex>,
    outgoing: String,
    incoming: String

}

impl UuidRule {
    pub fn new() -> UuidRule {
        UuidRule {
            regex: Some(Regex::new("uuid_regex", "/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i")),
            outgoing: "$(stub(UUID.randomUUID()), test(regex(uuid_regex))".to_string(),
            incoming: "$(stub(regex(uuid_regex)), test(UUID.randomUUID())".to_string()
        }
    }
}

impl Parsable for UuidRule {

    fn meets_rule(&self, val: &Value) -> bool {
        match val {
            Value::String(ref token) => {
                token.to_lowercase() == "uuid"
            }
            _ => false
        }
    }

    fn get_regex(&self) -> Option<Regex> {
        self.regex.clone()
    }

    fn get_outgoing(&self) -> &str {
        &*self.outgoing
    }

    fn get_incoming(&self) -> &str {
        &*self.incoming
    }
}



#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::Value;

    #[test]
    fn should_create_a_valid_uuid() {
        let uuid_rule = UuidRule::new();
        assert_eq!(uuid_rule.get_outgoing(), "$(stub(UUID.randomUUID()), test(regex(uuid_regex))".to_string());
        assert_eq!(uuid_rule.get_incoming(), "$(stub(regex(uuid_regex)), test(UUID.randomUUID())".to_string());
        assert_eq!(uuid_rule.get_regex(), Some(Regex::new("uuid_regex", "/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i")));
    }

    #[test]
    fn should_meet_rule_when_value_is_uuid() {
        let uuid_rule = UuidRule::new();
        let value = Value::String("uuid".to_owned());
        assert_eq!(uuid_rule.meets_rule(&value), true);
    }

    #[test]
    fn should_not_meet_rule_when_value_is_not_uuid() {
        let uuid_rule = UuidRule::new();
        let value = Value::String("boop".to_owned());
        assert_eq!(uuid_rule.meets_rule(&value), false);
    }

    #[test]
    fn should_not_meet_rule_if_it_not_type_string() {
        let uuid_rule = UuidRule::new();
        let bool_type = Value::Bool(false);
        assert_eq!(uuid_rule.meets_rule(&bool_type), false);
    }

}