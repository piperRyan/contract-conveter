#[derive(Serialize, Clone, PartialEq, Eq, Debug)]
pub struct Regex {
    pub name: String, 
    pub regex: String
}


impl Regex {
    pub fn new(name: &str, regex: &str) -> Self {
        Regex {
            name: name.to_owned(),
            regex: regex.to_owned()
        }
    }
}