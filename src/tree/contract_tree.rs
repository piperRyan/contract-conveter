use serde_json::{Value, Map};
use serde_json::value::Value::{Null, Object, Number, Bool, String as JsonString, Array};
use crate::rules::rules_engine::RulesEngine; 
use crate::rules::rule_types::RuleTypes;
use crate::rules::Parsable;
use crate::rules::regex::Regex;

pub struct ContractTree {
    rules_engine: RulesEngine,
    is_outgoing: bool,
    regexes: Vec<Regex>
}

impl ContractTree {
    pub fn new() -> Self {
        ContractTree {
            rules_engine: RulesEngine::new() ,
            is_outgoing: false,
            regexes: Vec::new()
        }
    }

    pub fn into_regexes(self) -> Vec<Regex> {
        self.regexes
    }

    pub fn visit(&mut self, json_val: &Value, is_outgoing: bool) -> String {
        let mut contract = String::new();
        self.is_outgoing = is_outgoing;
        self.visitor_helper(json_val, 4, &mut contract);
        contract.clone()
        
    }

    fn visitor_helper(&mut self, json_value: &Value, depth: usize, contract: &mut String) {
        match json_value {
            Object(ref token) => self.visit_object(token, depth, contract),
            Array(ref token) => self.visit_array(token, depth,  contract),
            JsonString(ref _token)=> self.visit_string(json_value,  contract),
            Number(ref _token)=> self.visit_number(json_value,  contract),
            Bool(ref _token)=> self.visit_bool(json_value,  contract),
            Null => panic!("No nulls should be found in contract!")
        }
    }

    fn visit_object(&mut self, token: &Map<String, Value>, depth: usize, contract: &mut String) {
        contract.push_str("[\n");
        let len = token.len() - 1;
        let mut count = 0; 
        for (key, value) in token.iter() {
            (1..depth + 1).for_each(|_| contract.push_str("\t"));
            contract.push_str(key);
            contract.push_str(": ");
            self.visitor_helper(value, depth + 1, contract);
            if count < len {
                contract.push_str(",");
            }
            contract.push_str("\n");
            count+=1
        }
        (1..depth).for_each(|_| contract.push_str("\t"));
        contract.push_str("]");
    }

    fn visit_array(&mut self, token: &Vec<Value>, depth: usize, contract: &mut String) {
        contract.push_str("[[\n");
        for value in token.iter() {
            (1..depth + 1).for_each(|_| contract.push_str("\t"));
            self.visitor_helper(value, depth + 1, contract);
        }
        contract.push_str("\n");
        (1..depth).for_each(|_| contract.push_str("\t"));
        contract.push_str("]]");
    }

    fn visit_string(&mut self, token: &Value, mut contract: &mut String) {
        let rules = self.rules_engine.get_rules_by_type(RuleTypes::String);
        add_primitive_rule(rules, &mut contract, token, self.is_outgoing, &mut self.regexes)
    }

    fn visit_number(&mut self, token: &Value, mut contract: &mut String) {
        let rules = self.rules_engine.get_rules_by_type(RuleTypes::Number);
        add_primitive_rule(rules, &mut contract, token, self.is_outgoing, &mut self.regexes)
    }


    fn visit_bool(&mut self, token: &Value, mut contract: &mut String) {
        let rules = self.rules_engine.get_rules_by_type(RuleTypes::Boolean);
        add_primitive_rule(rules, &mut contract, token, self.is_outgoing, &mut self.regexes)
    }
}


fn add_primitive_rule(rules: &Vec<Box<dyn Parsable>>, contract: &mut String,  token: &Value, is_outgoing: bool,  regex_var: &mut Vec<Regex>) {
    for rule in rules {
        let possible_rule = rule.into_rule(token, is_outgoing);
        if let Some(token) = possible_rule  { 
            if let Some(regex) = rule.get_regex() {
                regex_var.push(regex);
            } 
            contract.push_str(&*token);
            break;
        }
    }
}