use clap::{App, Arg, ArgGroup};
use clap::ArgMatches;
use converter_lib::command::contract_command::{ContractCommandBuilder};
fn main() {
    let matches = App::new("Contract Converter")
        .version("0.0.3")
        .about("Convert structured JSON to groovy contract")
        .arg(Arg::with_name("label")
            .short("l")
            .long("label-name")
            .required(true)
            .value_name("LABEL")
            .help("Set the label for this contract")
        )
        .arg(Arg::with_name("triggered_by")
            .short("t")
            .long("triggered-by")
            .value_name("TRIGGERED-BY")
            .help("If contract is to be triggered by a method use this argument. Provide method name without argument. Conflicts with message from option.")
        )
        .arg(Arg::with_name("json_path")
            .short("p")
            .long("json-path")
            .required(true)
            .value_name("PATH")
            .help("The file location of the json file which will be converted to a contract")
        )
        .arg(Arg::with_name("sent_to")
            .short("s")
            .long("sent-to")
            .required(true)
            .value_name("SENT-TO")
            .help("The name of the queue that the response message is sent to.")
        )
        .arg(Arg::with_name("message_from")
            .short("f")
            .long("message-from")
            .value_name("MESSAGE-FROM")
            .help("Then name of the queue that the request message is recevied from. Conflicts with triggered-by argument.")
        )
        .arg(Arg::with_name("write_to")
            .short("w")
            .long("write-to")
            .value_name("PATH")
            .required(false)
            .help("The directory to write the contract to. Defaults to the current directory the exe is in.")
        )
        .group(ArgGroup::with_name("input_message_group")
            .args(&["message_from", "triggered_by"])
            .required(true))
        .get_matches();
        build_command(matches);
}

fn build_command(matches: ArgMatches){
    let mut builder = ContractCommandBuilder::default();
    builder.label(matches.value_of("label").unwrap().to_string())
        .triggered_by(matches.value_of("triggered_by").map(|val| val.to_string()))
        .path_to_json(matches.value_of("json_path").unwrap().to_string())
        .message_from(matches.value_of("message_from").map(|val| val.to_string()))
        .sent_to(matches.value_of("sent_to").unwrap().to_string())
        .write_to(matches.value_of("write_to").map(|val| val.to_string()));
    builder.build().unwrap().run().unwrap();
}