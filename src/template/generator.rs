use tinytemplate::TinyTemplate;
use std::error::Error;
use crate::rules::regex::Regex;


static TEMPLATE: &'static str = 
"import org.springframework.cloud.contract.spec.Contract

{{ for regex in regexes  }}
def { regex.name } = \"{ regex.regex }\"; 
{{ endfor }}

Contract.make  \\{
    label(\"{ label }\")
    input \\{
        {{ if triggered_by }}
            triggeredBy('{ triggered_by }()')
        {{ else }}
            messageFrom '{ message_from }'
            body({ incoming | unescaped })
            headers \\{
                contentType(applicationJson())
            }
        {{ endif }}
    }    
    outputMessage \\{
        sentTo '{ sent_to }'
        body({ outgoing | unescaped })
        headers \\{
            contentType(applicationJson())
        }
    }
}";

#[derive(Serialize)]
pub struct TemplateGenerator {
    regexes: Vec<Regex>,
    incoming: Option<String>,
    outgoing: String,
    triggered_by: Option<String>,
    sent_to: String,
    label: String, 
    message_from: Option<String>
}

impl TemplateGenerator {
    pub fn new(
        regexes: Vec<Regex>, 
        incoming: Option<String>, 
        outgoing: String,
        label: String, 
        triggered_by: Option<String>,
        sent_to: String, 
        message_from: Option<String> 
    ) -> Self {
        TemplateGenerator {
            regexes: regexes,
            incoming: incoming,
            outgoing: outgoing,
            label: label,
            triggered_by: triggered_by,
            sent_to: sent_to,
            message_from: message_from
        }
    }

    pub fn into_contract(mut self) -> Result<String, Box<dyn Error>> {
        self.regexes.dedup_by(|x, y| x.name == y.name);
        let mut tt = TinyTemplate::new();   
        tt.add_template("contract", TEMPLATE)?;    
        let rendered = tt.render("contract", &self)?;
        Ok(rendered)
    }
}