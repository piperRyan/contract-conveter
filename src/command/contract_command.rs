use crate::io::reader::JsonReader;
use crate::tree::contract_tree::ContractTree;
use serde_json::Value;
use crate::template::generator::TemplateGenerator; 
use crate::io::writer::Writer;

#[derive(Builder, Debug)]
pub struct ContractCommand {
    triggered_by: Option<String>,
    path_to_json: String,
    label: String,
    message_from: Option<String>,
    sent_to: String,
    write_to: Option<String>
}

impl ContractCommand {
    pub fn run(self) -> Result<(), String> {
        let json = JsonReader::read_file(&*self.path_to_json)?;
        let mut tree = ContractTree::new();
        let incoming_body;
        let outgoing_body;
        match json {
           Value::Object(ref map) => {
                if map.contains_key("request") && self.triggered_by.is_none() {
                    incoming_body = Some(tree.visit(map.get("request").unwrap(), false));
                } else {
                    incoming_body = None;
                }
                outgoing_body = tree.visit(map.get("response").unwrap(), true);
           }, 
            _ => panic!("Invalid format should contain only objects at the top lvl.")   
        }
        let template_generator = TemplateGenerator::new(
            tree.into_regexes(), 
            incoming_body, 
            outgoing_body, 
            self.label, 
            self.triggered_by,
            self.sent_to,
            self.message_from
        );
        let contract = template_generator.into_contract().unwrap();
        Writer::write_contract(self.write_to, contract).unwrap();
        Ok(())
    }
}