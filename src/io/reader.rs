use serde_json::Value;
use std::path::Path;
use std::fs;

pub struct JsonReader;

impl JsonReader {
   pub fn read_file(path: &str) -> Result<Value, String> {
       let path_to_json = Path::new(path);
       if path_to_json.exists() {
           let raw_json = fs::read_to_string(path_to_json).map_err(|err| format!("Unable to read file: {}", err))?;
           serde_json::from_str(&*raw_json).map_err(|err| format!("{}", err))
       } else {
           Err(format!("File not found at {}", path))
       }
   }
}