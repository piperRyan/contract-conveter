use std::fs::create_dir_all;    
use std::fs::OpenOptions;
use std::io::Error;
use std::ffi::{OsStr, OsString};
use std::path::PathBuf;
use std::io::Write;

pub struct Writer; 


impl Writer {
    pub fn write_contract(path: Option<String>, contract: String) -> Result<(), Error> {
        let mut path_to_write;
        if let Some(path) = path {
            path_to_write = PathBuf::from(&path);
            if let Some(file_name) =  path_to_write.file_name() {
                let contract_name = file_name.to_owned();
                path_to_write.pop();
               create_parent_dir(&mut path_to_write, contract_name)?;
            } else {
                create_parent_dir(&mut path_to_write, OsString::from("contract.groovy"))?;           
            }
        } else {
            path_to_write = PathBuf::from(std::env::current_exe()?);
            path_to_write.set_file_name(OsStr::new("contract.groovy"));
        }
        let file = OpenOptions::new().write(true).truncate(true).create(true).open(path_to_write);
        let mut contract_file = file.unwrap();
        contract_file.write_all(contract.as_bytes())?;
        contract_file.sync_data()?;
        Ok(())
    }
}


fn create_parent_dir(path: &mut PathBuf, file_name: OsString) -> Result<(), Error> {
    if !path.exists() {
        create_dir_all(&path)?;
    }
    path.push(file_name);
    Ok(())
}